Chiwawa : Monitoring and protecting websites against file injection
###################################################################

:date: 2017-01-19
:modified: 2017-01-19
:tags: Python, Infosec, Web, Security, System, Joomla, Wordpress
:category: System
:slug: monitoring-and-protecting-websites-against-file-injection
:authors: Bruno VASTA
:headline: How to protect your website against file injection or file modification

Introduction
************

Few years ago, someone from a small company, in books publishing and selling, asked me if I was able to help them with a compromised website. When we was searching for the company name on search engines, the answer was not "we publish and sell books" but "we sell pills, drugs, viagra !". And also the website was sending spams...

.. class:: link-external

The attack was known as `"pharma hack" <http://whatis.techtarget.com/definition/pharma-hack>`_, it's targeting Joomla and Wordpress sites, exploiting a vulnerability in some extensions like TinyMCE WYSIWYG HTML editor embeded in those CMS.

I cleaned the website while deleting all "pharma hack" files and database entries, restored the compromised legit files, patched some files, deleted unsafe extensions, securised the web site with specialised extensions, and I hosted the site on my server to have more control on monitoring.

But I was aware it was not enough because the software running the site will be soon outdated and not anymore supported and the owner was not planning to migrate it on a fresh release.

Then I had to protect the site with a high level of security and paranoia.

.. class:: link-external

There is softwares for this, like `Naxsi WAF (Web Application Firewall) <https://github.com/nbs-system/naxsi>`_ , but I never installed it and was thinking it could take time before I could handle it. If you have time to learn, after reading this article, go ahead with Naxsi.

I wanted a solution to avoid file injection, if even the specialised extensions failed to block this kind of attack, and I wanted a quick solution, something tested in the morning and working before the end of the day.

The main goal
*************

File injection is the classical step in the process to attack on websites. The pirate find a way to inject a file and then remotly use it. Usualy it's a PHP file. Sometimes it's uploaded as an image, like a GIF file, then renamed and remotly used.

One simple way to prevent this it's to detect newly created PHP files in the website directories, and even to delete it when it appears.

Watcher
*******

At first I used Watcher, a Python script able to launch commands if some file movements are detected, but I was missing some features like path exclusion. Then I added this to Watcher and pushed the update in the Github repository.

.. class:: link-external

`Watcher's Github repository <https://github.com/gregghz/Watcher>`_

.. class:: link-external

Another thing I was not happy with Watcher is that it's working with `Inotify <https://en.wikipedia.org/wiki/Inotify>`_, the Linux Kernel's subsystem that notice changes in the system. Watcher don't works on BSD systems for exemple.

Chiwawa the Watchdog
********************

Then I coded Chiwawa, based on the Python library Watchdog, compatible with Linux and other systems likes BSD ones.

.. class:: link-external

`Chiwawa's Github repository <https://github.com/Brunus-V/chiwawa>`_

Chiwawa's Features
==================

- Detecting movements for files with extensions matching the list given to Chiwawa in it's file.
- Path exclusions to avoid monitoring caches or logs directories.
- Path inclusion.
- Alerts by mails if this mode is activated.
- Action mode or safe mode : in safe mode Chiwawa deletes nothing, just logging notices.
- Action mode ON : deleting target files for some kind of movements like creation.

Exemple of blocked attack
=========================

This is a Chiwawa log when it blocked an attack several monthes ago.

As we can see the attack targeted a Wordpress CMS while the targeted site is not a Wordpress.

The website has a security flow (patched after the attack) in it's code, and this was the open door for the injection.

::

	04-19 11:08 root         INFO     /var/www/site3/widgets.php created
	04-19 11:08 root         INFO     Deleted file : /var/www/site3/widgets.php
	04-19 11:08 root         INFO     /var/www/site3/widgets.php modified
	04-19 11:08 root         INFO     /var/www/site3/libraries/taxonomy.php created
	04-19 11:08 root         INFO     Deleted file : /var/www/site3/libraries/taxonomy.php
	04-19 11:08 root         INFO     /var/www/site3/libraries/taxonomy.php modified
	04-19 11:08 root         INFO     /var/www/site3/includes/bottom2-tpl.php created
	04-19 11:08 root         INFO     Deleted file : /var/www/site3/includes/bottom2-tpl.php
	04-19 11:08 root         INFO     /var/www/site3/includes/bottom2-tpl.php modified
	04-19 11:08 root         INFO     /var/www/site3/t3-assets/profile-lp.php created
	04-19 11:08 root         INFO     Deleted file : /var/www/site3/t3-assets/profile-lp.php
	04-19 11:08 root         INFO     /var/www/site3/t3-assets/profile-lp.php modified
	04-19 11:08 root         INFO     /var/www/site3/plugins/system/class-phpass-test.php created
	04-19 11:08 root         INFO     Deleted file : /var/www/site3/plugins/system/class-phpass-test.php
	04-19 11:08 root         INFO     /var/www/site3/plugins/system/class-phpass-test.php modified
	04-19 11:08 root         INFO     /var/www/site3/media/media/logo-wordpress-js.php created
	04-19 11:08 root         INFO     Deleted file : /var/www/site3/media/media/logo-wordpress-js.php
	04-19 11:08 root         INFO     /var/www/site3/media/media/logo-wordpress-js.php modified
	04-19 11:08 root         INFO     /var/www/site3/plugins/acymailing/locale-ext.php created
	04-19 11:08 root         INFO     Deleted file : /var/www/site3/plugins/acymailing/locale-ext.php
	04-19 11:08 root         INFO     /var/www/site3/plugins/acymailing/locale-ext.php modified
	04-19 11:09 root         INFO     /var/www/site3/plugins/user/ms-deprecated-js.php created
	04-19 11:09 root         INFO     Deleted file : /var/www/site3/plugins/user/ms-deprecated-js.php
	04-19 11:09 root         INFO     /var/www/site3/plugins/user/ms-deprecated-js.php modified
	04-19 11:09 root         INFO     /var/www/site3/plugins/hikashopshipping/profile-api.php created
	04-19 11:09 root         INFO     Deleted file : /var/www/site3/plugins/hikashopshipping/profile-api.php
	04-19 11:09 root         INFO     /var/www/site3/plugins/hikashopshipping/profile-api.php modified
	04-19 11:09 root         INFO     /var/www/site3/modules/mod_finder/admin-ext.php created
	04-19 11:09 root         INFO     Deleted file : /var/www/site3/modules/mod_finder/admin-ext.php
	04-19 11:09 root         INFO     /var/www/site3/modules/mod_finder/admin-ext.php modified
	04-19 11:09 root         INFO     /var/www/site3/components/com_acymailing/post.php created
	04-19 11:09 root         INFO     Deleted file : /var/www/site3/components/com_acymailing/post.php
	04-19 11:09 root         INFO     /var/www/site3/components/com_acymailing/post.php modified

How to launch Chiwawa as a daemon
=================================

.. class:: link-external

I'm using Supervisord `<http://supervisord.org/>`_ to start and stop Chiwawa as a daemon.

Side effects
============

Ok then just imagine you are Webmaster of a CMS like Joomla, you connect as super-admin and you see notices about pending upgrades in the backoffice GUI. Then, you forget to stop Chiwawa and you click on "Apply updates" from the backoffice GUI...what you think it will happen to the PHP files uploaded by the upgrade process ? :D


