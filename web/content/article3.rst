How to code MicroPython application on a WiPy microcontroler - part #2
######################################################################

:date: 2017-01-18
:modified: 2017-01-18
:tags: WiPy, MicroPython, microcontroler, DiY, Make, FabLab, Python, IoT
:category: Make
:slug: how-to-code-micropython-application-on-a-wipy-microcontroler-part-2
:authors: Bruno VASTA
:headline: With the exemple of a system to monitor temperature and publish it on a web page, it's easy and fun to learn how to use MicroPython on a WipY microcontroler.

Index
*****

.. class:: link-internal

- `Introduction`_
- `The wirering`_

  - `DS18B20 temperature probe`_
  - `Relay`_
  - `RGB led`_
  - `The WiPy`_

- `Let's code !`_

Introduction
************

In the article `How to code MicroPython application on a WiPy microcontroler - part #1 <{filename}/article2.rst>`_ we seen how to connect on a WiPy Microcontroller and how to upload and use libs and drivers.

Now we will see how to wire on the WiPy components like probes, relays, and leds.

What we will do it's to plug each cable or component's pins with the goos WiPy's pins.

.. image:: images/WiPy_1_DS18B20Probe_Realy_RGBLed.jpg
   :width: 800 px
   :alt: WiPy microcontroler wired with aDS18B20 temperature probe, a relay and a RGB led
   :align: center

Get back to `Index`_.

The wirering
************

Then we have to wire :

- DS18B20 temperature probe, 3 cables : ground, positive, data
- A relay, 3 pins : ground, positive, data
- A RGB led, 4 pins : ground, R, G, B

DS18B20 temperature probe
=========================

- ground cable on the WiPy's ground pin
- positive cable on the WiPy's 3V3 pin
- data cable (yellow) on the WiPy's pin #10 (GP10)

But for the temperature probe we need also to make a bridge, a pull-up, between the positive cable and the data cable with a 4,7Kohm resistor.

.. image:: images/DS18B20_probe.jpg
   :width: 800 px
   :alt: DS18B20 temperature probe wired to connect on a WiPy Microcontroler
   :align: center

*"The 4.7K ohm pull-up resistor is always connected between the power and the bus and holds the bus high unless either the master or one of the salves is actively pulling it down. This is handy because multiple devices can pull the bus low at the same time and nothing bad happens".*

.. class:: link-external

`Source : <https://wp.josh.com/2014/06/23/no-external-pull-up-needed-for-ds18b20-temp-sensor>`_.

I tested with an Arduino and the article author is right, we don't need the external pull-up because Arduino has an internal pull-up.

Get back to `Index`_.

Relay
=====

- ground cable on the WiPy's ground pin
- positive cable on the WiPy's 3V3 pin
- data cable (yellow) on the WiPy's pin #5 (GP5)

.. image:: images/relay.jpg
   :width: 800 px
   :alt: relay wired to connect on a WiPy Microcontroler
   :align: center

Get back to `Index`_.

RGB led
=======

- ground cable on the WiPy's ground pin
- R pin on the WiPy's pin #10 (GP7)
- G pin on the WiPy's pin #10 (GP8)
- B pin on the WiPy's pin #10 (GP9)

.. image:: images/RGB-led.jpg
   :width: 800 px
   :alt: RGB led wired to connect on a WiPy Microcontroler
   :align: center

Get back to `Index`_.

The WiPy
========

WiPy's ground and 3V3 pins are connected to the beadboard to be shared with the probe, the relay and the RGB led.

Pins #5, #7, #8, #9 and #10 are receiving directly the cable of the component they drive or are wired to the breadboard as a bridge to the component.

.. image:: images/WiPy-1.jpg
   :width: 800 px
   :alt: WiPy Microcontroler
   :align: center

.. image:: images/WiPy-1_and_RGB-led.jpg
   :width: 800 px
   :alt: relay wired to connect on a WiPy Microcontroler
   :align: center

Get back to `Index`_.

Let's code !
************

We will code something simple just to understand how it works with a MicroPython device like the WiPy.

This code should work if you save it as a lib in the WiPy /flash/lib memory card and import it, but, there is a bug I don't understand yet and it don't starts the loop.

To test it you could simply connect the WiPy, open the REPL session, **switch to copy/paste mode while typing CTRL+E**, and copy/paste the code. It will be executed when you will hit **CTRL+D to get out of the copy/paste mode**.

There is no other loop break than detecting a defined temperature, yes, it's dirty ;p . I should find later a better way to stop the loop, but this code it's just the first block of the whole system, and it's enough for a first and fast test.

.. code-block:: python

	# we will need some tempo
	import time

	# importing the onewire driver to work with the DS18B20 temperature probe
	import onewire

	# importing the WiPy's pin communication lib
	from machine import Pin

	# initialising the probe, testing device and temp reading
	ds = onewire.DS18X20(onewire.OneWire(Pin('GP10')))
	print('devices:', ds.roms)
	print('temperatures:', ds.read_temps())

	# initialising the relay
	relay = Pin('GP5', Pin.OUT, Pin.PULL_UP)
	relay.value(0)

	# initialising the RGB led chanels
	R_led = Pin('GP7', Pin.OUT, Pin.PULL_UP)
	G_led = Pin('GP8', Pin.OUT, Pin.PULL_UP)
	B_led = Pin('GP9', Pin.OUT, Pin.PULL_UP)
	R_led.value(0)
	G_led.value(0)
	B_led.value(0)

	# temperature values for lowest, low, medium, high, and highest breakpoints
	# 2000 means 20.00°C
	temp_values=(2000, 2200, 2400, 2500, 2600)

	def stop_probe():
		relay.value(0)
		R_led.value(0)
		G_led.value(0)
		B_led.value(0)
		return()

	def start_probe(temp_lowest, temp_low, temp_medium, temp_high, temp_highest):
		while True:
			
			temp = ds.read_temps()
			print (temp[0])

			# Relay is open on temp_high detected
			if temp[0] > temp_high and relay.value() == 0:
				relay.value(1)
			if temp[0] <= temp_high and relay.value() == 1:
				relay.value(0)

			# B channel of the RGB led is activated on temp_lowest
			if temp[0] >= temp_lowest and B_led.value() == 0:
				B_led.value(1)
			if temp[0] < temp_lowest and B_led.value() == 1:
				B_led.value(0)

			# G channel of the RGB led is activated on temp_low
			if temp[0] >= temp_low and G_led.value() == 0:
				G_led.value(1)
			if temp[0] < temp_low and G_led.value() == 1:
				G_led.value(0)

			# R channel of the RGB led is activated on temp_medium
			if temp[0] >= temp_medium and R_led.value() == 0:
				R_led.value(1)
			if temp[0] < temp_medium and R_led.value() == 1:
				R_led.value(0)

			# on temp_highest detected we reset the components and kill the loop
			if temp[0] >= temp_highest :
				temp[0] = 0
				stop_probe()
				break

			time.sleep(1)

	if __name__ == "__main__":

		start_probe(temp_values[0], temp_values[1], temp_values[2], temp_values[3], temp_values[4])	


Get back to `Index`_.