How to code MicroPython application on a WiPy microcontroler - part #1
######################################################################

:date: 2017-01-16
:modified: 2017-01-16
:tags: WiPy, MicroPython, microcontroler, DiY, Make, FabLab, Python, IoT
:category: Make
:slug: how-to-code-micropython-application-on-a-wipy-microcontroler-part-1
:authors: Bruno VASTA
:headline: With the exemple of a system to monitor temperature and publish it on a web page, it's easy and fun to learn how to use MicroPython on a WipY microcontroler.

Index
*****

.. class:: link-internal

- `Introduction`_
- `The main goal`_
- `Connecting on the WiPy , updating the firmware and uploading the OneWire driver (for the DS18B20 probe)`_
  
  - `WiPy's wifi network`_
  - `Telnet service`_
  - `FTP service`_
  - `Updating the firmware`_
  - `How to install a lib or a driver on WiPy and use it`_
  
    - `How to import a lib or a driver on WiPy`_
    - `How to use a driver on the WiPy`_

- `What next ?`_

Introduction
************

I wanted to monitor and adjust the temperature of my fish-tank, and to publish the temperature levels on a wab page.

To do this it's possible to use a temperature probe and a relay to on/off the fish-thank heater and of course a microcontroler to drive the whole thing.

First I tryed with an Arduino microcontroler, but coding for Arduino means Java, and i'm much more easy with Python.

The WiPi microcontroller has :

- MicroPython firmware and drivers.
- Integrated WiFy chipset.
- 3.3V input/output instead of 5.5V on Arduino.
- A MicroPhyton console behind a telnet service, wich makes the device very friendly while in test/dev sessions.

.. class:: link-external

I bought a WiPy microcontroler : `WiPy things on MC Hobby online shop <https://shop.mchobby.be/recherche?controller=search&orderby=position&orderway=desc&search_query=Wipy&submit_search=>`_

And here is a pic of the whole system (it's a test release) :

.. image:: images/WiPy_1_DS18B20Probe_Realy_RGBLed.jpg
   :width: 800 px
   :alt: WiPy microcontroler wired with aDS18B20 temperature probe, a relay and a RGB led
   :align: center

What we can see on this pic :

- WiPy 1 on his extension board.
- The DS18B20 probe's head at the bottom of the pic, just above the breadboard.
- The DSA8B20 terminal block with a 4,7Kohm resistor
- The relay, almost at center of the pic, with it's rainbow wires.
- The RGB Led directly connected on the breadboard.

.. class:: link-internal

Get back to `Index`_.

The main goal
*************
We want : The WiPy to pull the temperature from the DS18B20 probe, then, in function of the temperature, open/close the relay, and ON/OFF the R, G, and B channels of the RGB Led.

Note that the RGB Led si only there to show a some kind of visual notifications, but it should be replaced with a LCD screen to display the temperature...let's keep that for later...

Connecting on the WiPy , updating the firmware and uploading the OneWire driver (for the DS18B20 probe)
*******************************************************************************************************
To boot on the WipY, you can plug in on a 3,3V source through it's USB micro connector, or from your computer USB.

Once booted, you can access the WiPy from your computer with 3 services :

- Wifi network : the WiPy has it's own Wifi network setted by default but it can be connected to any Wifi network.
- Telnet service to use the REPL (Micro Python Embeded Console)
- FTP service to upload firmwares, drivers, and your code.

.. class:: link-internal

Get back to `Index`_.

WiPy's wifi network
===================
Off course, to be able to connect Telnet or FTP services one must first connect the WiPy through Wifi network.

To connect the WiPy using the Wifi SSID and default settings :

- SSID is the one starting with  : wipy-lan
- Wifi key it's : www.wipy.io

.. pull-quote ::
	The WiPy's default SSID, wifi key, login and password can be setted in the WiPy's configuration file.
	And while WiPy is a IoT it's very important to change, at least, the default credentials !

Note that it's also possible to set the WiPy to be connected to a home Wifi network, like other home devices.

.. class:: link-internal

Get back to `Index`_.

Telnet service
==============
Once you have connected the Wify network of the WiPy you can connect the MicroPhyton console like this, using the default credentials :

- telnet 192.168.1.1
- login : micro
- password : python

Then it should answer you and display a Version Number for the firmware.

And also, you are in interactiv mode with the REPL (Python Console), then you can start testing Python things like ::

	>>> import time
	>>> print('hello World !')
	>>> time.sleep(3)
	>>> r = 4 / 2   # will not work while WiPy 1 don't handle floats
	>>> r = 4 // 2  # will work
	>>> ... more Python things

.. class:: link-internal

Get back to `Index`_.

FTP service
===========
To upload files on the WiPy you should use the FTP service provided by the device.
Ton connect the FTP service, it's like for the Telnet service :

- ftp 192.168.1.1
- login : micro
- password : python

WiPy's FTP service is in passive mode, then it's needed to switch the FTP client in passive mode while taping : passive.

.. class:: link-internal

Get back to `Index`_.

Updating the firmware
=====================

.. class:: link-external

- The last firmwares for the WiPy Boards can be loaded from `MicroPython.org <http://micropython.org/download>`_.
- Connect the FTP service of the WiPy and put the mcuimg.bin file from the firmware archive in the directory /flash/sys/mcuimg.bin.
- Reboot the WiPy with the reset button or by typing :

::
	
	>>> import machine
	>>> machine.reset()

.. class:: link-internal

Get back to `Index`_.

How to install a lib or a driver on WiPy and use it
===================================================

How to import a lib or a driver on WiPy
---------------------------------------

I need to downloading and import the OneWire Driver for the DS18B20 probe

.. class:: link-external

I got the driver from the Micro Python Forum : `OneWire driver for WiPy <http://forum.micropython.org/download/file.php?id=135>`_

There is other sources for this driver, but the one I'm provinding there works with WiPy 1.x and stock firmware. I'll test later with a WiPy 2.0, I ordered one and I'm waiting for it.

How to use a driver on the WiPy
-------------------------------
- Upload it with FTP on in the directory /flash/lib.
- Import it from REPL or from your code.

Importing a driver like onewire.py is easy, for exemple in the REPL whle typing :

::
	
	>>> import onewire

.. epigraph::

   No matter where you go, there you are.

.. class:: epigraph-author

Buckaroo Banzai

.. class:: link-internal

Get back to `Index`_.

What next ?
***********

In the next article I'll explain how to wire the WiPy, the probe, the relay anc the RGB led.
`How to code MicroPython application on a WiPy microcontroler - part #2 <{filename}/article3.rst>`_ 

.. class:: link-internal

Get back to `Index`_.