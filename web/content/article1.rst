Meetup : Web Fonts and Icons Fonts
##################################

:date: 2017-01-12
:modified: 2017-01-16
:tags: CSS, HTML, fonts, icons, SVG, Web, design
:category: WebDev
:slug: meetup-web-fonts-and-icons-fonts
:authors: Bruno VASTA
:headline: How to use Web Fonts and Web Icons without slowing down your pages load


This is the first article of this blog.

Yesterday evening I had a talk, a Meetup, I was invited to talk about Web fonts, Web Icons, SVG and Web performances.

The presentation is actually in French, but I'll translate it later.

The whole presentation is in the archive `Meetup Web Fonts et Icons Fonts <{filename}/files/meetup_webfonts-et-iconsfont_11-01-2017.zip>`_

In short, what was the content of this speech :

- Why we need fonts for italic and bold styles and why we should not let the browser render italic and bold from a regular style.
- Why we should avoid using Google Font CDN or any other CDN to load libraries and fonts in webpages.
- How to load a free font from the website tree.
- How to base64 encode a font and import it in the CSS.
- How to use Web fonts like FontAwesome.
- How to use font icons as SVG images.
- How to use Web fonts kit generator like Fontello to package icons.
