#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Bruno VASTA'
SITENAME = u'Brunus Lab'
SITEURL = ''
SITEDESCRIPTION = 'Coding, designing, making things, sharing...'

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DATE_FORMAT = '%-d/%-m/%Y '

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('PauLLA', 'http://www.paulla.asso.fr/'),
         ('MIPS lab', 'http://www.mips-lab.net/'),
         ('Meetup AgnosTech PAU', 'https://www.meetup.com/fr-FR/AgnosTech-Pau/'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True

STATIC_PATHS = ['images', 'files']
